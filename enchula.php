<html>
	<?php include_once('header.php'); ?>
<body>
  <header>
		<?php
				if(isset($_GET['respuesta']) && isset($_GET['nombre']) && isset($_GET['email']) ){
						$array = [",",".","*","<",">","/","-","+","%"];
						$respuesta =  str_replace($array,"",strip_tags($_GET['respuesta']));
						$array = [",",".","*","<",">","/","-","+","%"];
						$nombre = str_replace($array,"",strip_tags($_GET['nombre']));
						$array = [",",".","*","<",">","/","-","+","%"];
						$email = str_replace($array,"",strip_tags($_GET['email']));
				}
		?>
  </header>
	<div class="velo" style="display:none;position:fixed; width:100%; height:100%; z-index:999; background:rgba(0,0,0,0.7);">
			<img style="width:150px; height 150px; left:0; right:0; margin:0 auto; top:30vh; position:absolute;" src="images/loader.gif" alt="gif" />
	</div>
  <div class="content enchula">
    <div class="centered-leafs">
    	<img src="images/hojas-trazos.png" draggable="false" alt="">
    </div>
		<div class="content-holder">
				<header>
					<a href="javascript:void(0);"><img class="titulo" src="images/titulo-peq.png" draggable="false" alt="pizarra ganadora"></a>
				</header>
				<div class="pizarron" style="text-align:center; display:none;">
					<h1>#RespuestasGanadoras</h1>
					<img style="width:65px; height:65px; margin: 0 auto 0 auto;" src="images/Emoji Smiley-139.png" alt="">
						<!--canvas id="canvas" width="300" height="300"></canvas-->
						<p style="margin-top:4vh; color:white;"><?php echo $respuesta  ?><p>
				</div>
				<p style="text-align:center; margin-bottom:25px;">
					Selecciona tus 5 emojis preferidos, y muévelos donde prefieras
				</p>
				<canvas style="display:block; margin: 0 auto;" id="canvase" width="900" height="350">

				</canvas>
				<div class="respuesta">
					<div class="emoji  flexslider">
						<ul class="slides">
							<li><img src="images/emojis/1.png" alt=""></li>
							<li><img src="images/emojis/2.png" alt=""></li>
							<li><img src="images/emojis/3.png" alt=""></li>
							<li><img src="images/emojis/4.png" alt=""></li>
							<li><img src="images/emojis/5.png" alt=""></li>
							<li><img src="images/emojis/6.png" alt=""></li>
							<li><img src="images/emojis/7.png" alt=""></li>
							<li><img src="images/emojis/8.png" alt=""></li>
							<li><img src="images/emojis/9.png" alt=""></li>
							<li><img src="images/emojis/10.png" alt=""></li>

							<li><img src="images/emojis/11.png" alt=""></li>
							<li><img src="images/emojis/12.png" alt=""></li>
							<li><img src="images/emojis/13.png" alt=""></li>
							<li><img src="images/emojis/14.png" alt=""></li>
							<li><img src="images/emojis/15.png" alt=""></li>
							<li><img src="images/emojis/16.png" alt=""></li>
							<li><img src="images/emojis/17.png" alt=""></li>
							<li><img src="images/emojis/18.png" alt=""></li>
							<li><img src="images/emojis/19.png" alt=""></li>
							<li><img src="images/emojis/20.png" alt=""></li>

							<li><img src="images/emojis/21.png" alt=""></li>
							<li><img src="images/emojis/22.png" alt=""></li>
							<li><img src="images/emojis/23.png" alt=""></li>
							<li><img src="images/emojis/24.png" alt=""></li>
							<li><img src="images/emojis/25.png" alt=""></li>
							<li><img src="images/emojis/26.png" alt=""></li>
							<li><img src="images/emojis/27.png" alt=""></li>
							<li><img src="images/emojis/28.png" alt=""></li>
						</ul>
					</div>
				</div>
				<div class="buttons-holder">
				                    <button onClick="javascript:location.reload();" class="clearEmoji">Borrar emojis</button>
				                    <button class="gotoEnvia">Siguiente</button>
				                </div>
				<img class="vaso" src="images/vaso-completo.png" alt="">
		</div>
	</div>
	<script type="text/javascript">
			$('.velo').fadeIn();
$(window).load(function(){
		var canvas = document.getElementById("canvase");
		var ctx = canvas.getContext("2d");



		var metrica;

		var isMouseDown = false;
		var startX;
		var startY;
		var dragX;
		var dragY;
		var p = 0;
		var items = []
		var titulo;
		var respuestaCanvas;
		var fondo = new Image();
		var centerX;
		var centerY;
		var icono = new Image();
		/*var obj = {
				imagen: new Image(),
				_x: 0,
				_y: 0
		}*/
		var arr = [];

		$('.velo').fadeIn();
		titulo = $('.pizarron h1').html();
		respuestaCanvas = '<?php echo $respuesta; ?>';
		ctx.font = "bold 26px 'chalk' ";

		if($(window).width() <= 768){
				width = $(window).width();
				canvas.width = width;
				var canvasOffset = $("#canvase").offset();
				var offsetX = canvasOffset.left;
				var offsetY = canvasOffset.top;
				ctx.font = "bold 22px 'chalk' ";
		}else if($(window).width() >= 769 && $(window).width() <= 1367) {
				width = 900;
				canvas.height = 250;
				var canvasOffset = $("#canvase").offset();
				var offsetX = canvasOffset.left;
				var offsetY = canvasOffset.top;
				ctx.font = "bold 26px 'chalk' ";
		}else{
			width = 900;
			var canvasOffset = $("#canvase").offset();
			var offsetX = canvasOffset.left;
			var offsetY = canvasOffset.top;
			ctx.font = "bold 26px 'chalk' ";
		}
		ctx.fillStyle = "white";
		centerX = canvas.width/2;
		centerY = canvas.height/2;
		fondo.src = 'images/chalk-texture.png';

		fondo.onload = function(){
			setTimeout(function(){
					putContent();
			},4000);
		}
		$('.emoji .slides li').click(function(){
				var x = Math.floor((Math.random() * canvas.width) + 1);
				var y = Math.floor((Math.random() * canvas.height) + 1);
				var img = $(this).children('img').attr('src');
				var temp;

				$('.pizarron').children('img').attr('src',img);
				//icono.src = img;

					temp = new Image();
					temp.src = img;

				/*obj.imagen.src = img;
				obj._y = y;
				obj._x = x;
				if(p <= 5){
					arr[p]= obj;
				}else{
					p--;
				}*/


				temp.onload = function(){
					if(p <= 5){
						//drawEmojis(x, y , icono);
						drawEmojis(x, y , temp);
					}
			 	}
				 p++;
		});
		function drawEmojis(x, y ,img){
			//for (var i = 0; i < arr.length; i++) {
				//contexto.drawImage(arr[i].imagen,arr[i]._x,arr[i]._y,80,80);
				items.push({
				    x: x,
				    y: y,
				    width: 80,
				    height: 80,
				    color: "",
				    isDragging: false,
						image: img
				});
			//}
					draw();
		}
		draw();

		function putContent(){
			var start = 0; // Carácter por el que empezar
			var currentLine = 0; // Línea en la que nos encontramos.
			var jumpAt = 55; // Número de caracteres por línea.
			var lineHeight = 25;
			var mtr = ctx.measureText(titulo);
			ctx.drawImage(fondo,0,0,width,350);
			ctx.fillText(titulo,centerX-mtr.width/2,centerY-90);
		//	centerY = centerY - 45;
			while (start < respuestaCanvas.length)
			{
					str = respuestaCanvas.substr(start,jumpAt);
					metrica = ctx.measureText(str);
					start += jumpAt;

					ctx.fillText(str, centerX-metrica.width/2 , 150+(lineHeight*currentLine++));
			}
			//ctx.fillText(respuestaCanvas,centerX-metrica.width/2,centerY);
		}

		function draw() {
		    ctx.clearRect(0, 0, canvas.width, canvas.height);
				putContent();
		    for (var i = 0; i < items.length; i++) {
		        var item = items[i];
		        if (isMouseDown) {

		        }
		        ctx.beginPath();
		        if (item.isDragging) {
		            //ctx.rect(item.x + dragX, item.y + dragY, item.width, item.height);
								ctx.drawImage(item.image,item.x + dragX, item.y + dragY, item.width, item.height);
		        } else {
		            //ctx.rect(item.x, item.y, item.width, item.height);
								ctx.drawImage(item.image,item.x, item.y, item.width, item.height);
		        }
		        //ctx.fillStyle = item.color
		        ctx.fill();
		    }
		}


		function setDragging(x, y) {
		    for (var i = 0; i < items.length; i++) {
		        var item = items[i];
		        if (x >= item.x && x <= item.x + item.width && y >= item.y && y <= item.y + item.height) {
		            item.isDragging = true;
		        }
		    }
		}

		function clearDragging(x, y) {
		    for (var i = 0; i < items.length; i++) {
		        var item = items[i];
		        if (item.isDragging) {
		            items[i].isDragging = false;
		            item.x += dragX;
		            item.y += dragY;
		        }
		    }
		}

		function handleMouseDown(e) {
		    mouseX = parseInt(e.clientX - offsetX);
		    mouseY = parseInt(e.clientY - offsetY);

		    // Put your mousedown stuff here
		    startX = mouseX;
		    startY = mouseY;
		    setDragging(startX, startY);
		    isMouseDown = true;
		}

		function handleMouseUp(e) {
		    mouseX = parseInt(e.clientX - offsetX);
		    mouseY = parseInt(e.clientY - offsetY);

		    // Put your mouseup stuff here
		    isMouseDown = false;
		    clearDragging();
		}

		function handleMouseOut(e) {
		    mouseX = parseInt(e.clientX - offsetX);
		    mouseY = parseInt(e.clientY - offsetY);

		    // Put your mouseOut stuff here
		    isMouseDown = false;
		    clearDragging();
		}

		function handleMouseMove(e) {
		    mouseX = parseInt(e.clientX - offsetX);
		    mouseY = parseInt(e.clientY - offsetY);

		    // Put your mousemove stuff here
		    if (isMouseDown) {
		        dragX = mouseX - startX;
		        dragY = mouseY - startY;
		        draw(mouseX, mouseY);
		    }

		}
	//mobile
	function handleTouchDown(e) {
		  $('.content-holder').addClass('ovhidden');
			mouseX = parseInt(e.touches[0].pageX - offsetX);
			mouseY = parseInt(e.touches[0].pageY - offsetY);

			// Put your mousedown stuff here
			startX = mouseX;
			startY = mouseY;
			setDragging(startX, startY);
			isMouseDown = true;
	}

	function handleTouchOut(e) {
		 $('.content-holder').removeClass('ovhidden');
		 $('.content-holder').scrollTop(0);
			mouseX = parseInt(e.changedTouches[0].pageX - offsetX);
			mouseY = parseInt(e.changedTouches[0].pageY - offsetY);

			// Put your mouseOut stuff here
			isMouseDown = false;
			clearDragging();
	}

	function handleTouchMove(e) {
			mouseX = parseInt(e.touches[0].pageX - offsetX);
			mouseY = parseInt(e.touches[0].pageY - offsetY);

			// Put your mousemove stuff here
			if (isMouseDown) {
					dragX = mouseX - startX;
					dragY = mouseY - startY;
					draw(mouseX, mouseY);
			}

	}

		$("#canvase").mousedown(function (e) {
		    handleMouseDown(e);
		});
		$("#canvase").mousemove(function (e) {
						console.log(e);
		    handleMouseMove(e);
		});
		$("#canvase").mouseup(function (e) {
		    handleMouseUp(e);
		});
		$("#canvase").mouseout(function (e) {
		    handleMouseOut(e);
		});
		//mobile
			canvas.addEventListener("touchstart", function (e) {
				console.log(e);
					handleTouchDown(e);
			}, false);
			canvas.addEventListener("touchmove", function (e) {
		//		console.log(e);
					handleTouchMove(e);
			}, false);
			canvas.addEventListener("touchend", function (e) {
					handleTouchOut(e);
			}, false);
/*		$("#canvase").on('touchstart',function (e) {
			console.log(e);
				handleMouseDown(e);
		});
		$("#canvase").on('touchmove',function (e) {
				handleMouseMove(e);
				console.log(e);
		});
		$("#canvase").on('touchend',function (e) {
			console.log(e);
				handleMouseUp(e);
		});*/

		$('.gotoEnvia').click(function(){
			$('.velo').fadeIn();
				var resp = $('.pizarron p').html();
				var email = "<?php echo $email; ?>";
				var nombre = "<?php echo $nombre; ?>";
				var imagenPizarra = canvas.toDataURL("image/png").replace('data:image/png;base64,', '');
				$.post("inc/guardaImagen.php",{data:imagenPizarra,correo:email,respuesta:resp},function(server){
						console.log(server);
						if(server.imagen != false && server.respuesta)
						{
							imgShare = server.imagen;
							$('.acepto').fadeOut();
							$('.compartir').fadeIn();
							$('.velo').fadeOut();
							//window.location.href='envia.php?respuesta='+resp+'&correo='+email+'&nombre='+nombre+'&imagen='+imgShare;
							window.location.href='envia.php?imagenUrl='+imgShare+'&resp='+resp+'&email='+email+'&nombre='+nombre;
						}
				},'json');
		});
				$('.velo').fadeOut();
});
	</script>
  <footer>
		<div class="modal">
			<section class="alert1">
				<i class="fa fa-close"></i>
				<h1>¡ HEY Bájale 2!</h1>
				<h3>Esta pregunta se activará la semana que viene</h3>
				<img class="vaso" src="images/vaso-completo.png" alt="">
			</section>
		</div>
  </footer>
</body>
