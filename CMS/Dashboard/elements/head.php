<meta http-equiv="Content-Type" content="text/html,charset=UTF-8" />
<title>PIZARRA NESTEA CMS</title>
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link href="./css/styles_admin.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="./js/main_admin.js"></script>
