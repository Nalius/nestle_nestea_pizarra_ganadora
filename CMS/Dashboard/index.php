<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include_once 'elements/head.php';?>
    </head>
    <?php include_once 'inc/init.php'; ?>
    <body>
        <?php include_once'elements/menu.php'; ?>
        <section>
            <div id="contenido">
                <?php include_once 'content.php';?>
            </div>
        </section>
    </body>
</html>
