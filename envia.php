<html>
	<?php include_once('header.php'); ?>
<body>
  <header>
		<?php
			if(isset($_GET['imagenUrl']) && isset($_GET['resp']) && isset($_GET['email']) && isset($_GET['nombre'])){
					$array = [",","*","<",">","/","-","+","%"];
					$imagenUrl= str_replace($array,"",strip_tags($_GET['imagenUrl']));
					$array = [",",".","*","<",">","/","-","+","%"];
					$resp =  str_replace($array,"",strip_tags($_GET['resp']));
					$array = [",",".","*","<",">","/","-","+","%"];
					$email = str_replace($array,"",strip_tags($_GET['email']));
					$array = [",",".","*","<",">","/","-","+","%"];
					$nombre = str_replace($array,"",strip_tags($_GET['nombre']));
			}
		?>
  </header>
	<div class="velo" style="display:none;position:fixed; width:100%; height:100%; z-index:999; background:rgba(0,0,0,0.7);">
			<img style="width:150px; height 150px; left:0; right:0; margin:0 auto; top:30vh; position:absolute;" src="images/loader.gif" alt="gif" />
	</div>
  <div class="content enviar">
    <div class="centered-leafs">
    	<img src="images/hojas-trazos.png" draggable="false" alt="">
    </div>
		<div class="content-holder">
				<header>
					<a href="index.php"><img class="titulo" src="images/titulo-peq.png" draggable="false" alt="pizarra ganadora"></a>
				</header>
				<div class="pizarron" style="display:block;">
					<img src="pizarras/<?php echo $imagenUrl; ?>" alt="" />
					<!--h1>#respuestaGanadora</h1>
						<div id="canvas">
							<figure><img src="pizarras/<?php //echo $imagenUrl; ?>" alt=""></figure>
							<p><?php //echo $resp; ?></p>
						</div>
				</div-->
				<canvas style="display:none; margin: 0 auto;" id="canvase" width="800" height="350">

				</canvas>
				<div class="button-holder">
					<button onClick="javascript:window.history.back();" >Volver</button>
					<button class="start">Enviar</button>
				</div>
				<img class="vaso" src="images/vaso-completo.png" alt="">
		</div>
	</div>
  <footer>
		<div class="modal">
			<section class="legal">
				<i class="fa fa-close"></i>
				<h1 class="tyc">Términos y Condiciones</h1>
				<div class="terminos">
					<p>1. Los participantes deben ser fans del page de NESTEA® en Facebook, de lo contrario no podrán seguir al siguiente paso.</p>
					<p>2. Lineamientos a respetar para que tu #RespuestaGanadora sea aprobada y publicada:</p>
					<p>- Tu #RespuestaGanadora no puede superar de las 4 líneas.</p>
					<p>- No está permitido el uso de groserías.</p>
					<p>- No está permitido el uso de contenido discriminatorio asociado a temas raciales, sexuales, políticos o religiosos. </p>
					<p>3. Cada usuario puede publicar cuantas Respuestas Ganadoras desee, no hay límite.</p>
					<p>4. NESTEA® escogerán las 3 mejores Respuestas Ganadoras y se publicarán en nuestro Fan Page para que los usuarios voten por la mejor. La ganadora se elegirá con 60% likes en la publicación y  40% nuestro criterio.</p>
					<p>5. Los participantes deben ser mayores de 18 años. </p>
					<p>6. Los participantes autorizan a NESTEA® a difundir y reproducir de forma gratuita en las Redes Sociales y demás medios de comunicación todo el material fijado voluntariamente en el sitio (fotografías y video), incluso para realizar actividades de material promocional o publicitario relacionadas con la marca NESTEA®.</p>
					<p>7. NESTEA® tiene la potestad de borrar comentarios indebidos que muestren cualquier actuación deshonesta (incluye contenido obsceno o agresivo) y actuaciones ofensivas hacia los demás participantes o miembros de NESTEA®.</p>
					<p>8. La actividad estará abierta a todos los ciudadanos que tengan un perfil válido en Facebook.</p>
					<p>9. Facebook no patrocina, avala, ni administra de modo alguno este evento, ni está asociado a él. Los participantes son conscientes que están proporcionando su información a NESTEA® y no a Facebook. </p>
					<p>10. NESTEA® no patrocina, avala, apoya ni administra de modo alguno los comentarios personales asociados a las cuentas de Facebook de cada usuario. Cada usuario es responsable de su mensaje a compartir. </p>
					<p>11. NESTEA® se reserva el derecho de aclarar, complementar o modificar estos términos y condiciones, en cualquiera de sus partes y en cualquier momento.</p>
				</div>
				<div style="display:none; text-align:center;" class="listos">
					<p style="font-size:30px">¡Tu respuesta ha sido enviada! si lo deseas puedes compartirla en tu muro de Facebook.</p>
				</div>
				<button class="acepto">Acepto</button>
				<button style="display:none;" class="compartir">Compartir</button>
			</section>
		</div>
		<script type="text/javascript">
		$(document).ready(function(){
			var imgShare = $('.pizarron').children('img').attr("src");
			function shareFacebook(){
				console.log('https://hacemosloquenosgusta.com/lab/pizarra-nestea/'+imgShare);
					FB.api('/me/photos', 'post',{message:'<?php echo $resp; ?>',url:'https://hacemosloquenosgusta.com/lab/pizarra-nestea/'+imgShare,access_token:'EAAP5I1FlDB8BAI9bAmkTtosOa9pRZB02SZAd7uGudB294tNuOTxjljo3rOaXHBfORtDxQU27XxEQZBJe6HyiP1jYQWQLgsSQjtRNo3qPUYZBFtyGIL9i7xoUkqLKKZBA2UYFexZBSAhyiAIrPZADWZCAQGMntPmJzw8ZD'}, function(response) {
							 if (!response || response.error) {
									console.log(response);
									alert('Hubo un error al compartir');
							 } else {
									console.log(response);
									if (response.post_id != null) {
											alert('REVISA TU MURO');
											window.location.href='index.php';
									}
							 }
							 $('.velo').fadeOut();
				  });
			}
			function loginFacebook(){
			  FB.getLoginStatus(function(response) {
			      statusChangeCallback(response);
			  });
			}
			function statusChangeCallback(response) {
			  if (response.status == 'connected') {
			    FB.api('/me',{fields:'first_name,last_name,email'},function(user){
			      shareFacebook();
			    });
			  } else if (response.status == 'not_authorized') {
			    FB.login(function(login){
			      FB.api('/me',{fields:'first_name,last_name,email'},function(user){
			          shareFacebook();
			      });
			    },{scope:'email,public_profile,user_photos,publish_actions,user_photos'});
			  }else{
			    FB.login(function(login){
			      FB.api('/me',{fields:'first_name,last_name,email'},function(user){
			        shareFacebook();
			      });
			    },{scope:'email,public_profile,publish_actions,user_photos'});
			  }
			}
			$('.acepto').click(function(){
				$(this).fadeOut();
				$('.velo').fadeIn();
				$('.compartir').fadeIn();
				$('.velo').fadeOut();
				$('.tyc').fadeOut();
				$('.terminos').fadeOut();
				$('.listos').fadeIn();
				$.post( "inc/sendmail.php",{msg:'<?php echo $resp ?>'},function( data ) {
				  console.log(data);
				});
			});
			$('.compartir').click(function(){
				$('.velo').fadeIn();
					loginFacebook();
			});
		});
		</script>
  </footer>
</body>
