<html>
	<?php include_once('header.php'); ?>
<body>
  <header>
		<?php
				$pizarra = new Pizarra();
				$pregunta = $pizarra->buscarPregunta();
				$visible = $pizarra->visible();
			//	print_r($visible);
		?>
  </header>
  <div class="content preguntas">
    <div class="centered-leafs">
    	<img src="images/center-leafs.png" draggable="false" alt="">
    </div>
		<div class="content-holder">
				<header>
					<a href="index.php"><img class="titulo" src="images/titulo-peq.png" draggable="false" alt="pizarra ganadora"></a>
					<p>¿Vas pendiente de un Google Cardboard y un teléfono inteligente? ¡Actívate! Contesta esta pregunta, envía tu #RespuestaGanadora y participa</p>
				</header>
				<div class="pizarras">
					<div class="pizarra-holder">
						<figure class="pizarrita">
							<p>
									<?php
											echo ($visible['posicion'] == 1) ? $pregunta['pregunta'] : '<p class="closeda" style="font-size:100px;">?</p>';
									?>
							</p>
							<img class="o-piz" src="images/tiza-ornaments.png" draggable="false" alt="">
						</figure>
						<figure class="pizarrita">
							<p>
									<?php
											echo ($visible['posicion'] == 2) ? $pregunta['pregunta'] : '<p class="closedb" style="font-size:100px;">?</p>';
									?>
							</p>
							<img class="o-piz" src="images/tiza-ornaments.png" draggable="false" alt="">
						</figure>
						<figure class="pizarrita">
							<p>
									<?php
											echo ($visible['posicion'] == 3) ? $pregunta['pregunta'] : '<p class="closedc" style="font-size:100px;">?</p>';
									?>
							</p>
							<img class="o-piz" src="images/tiza-ornaments.png" drggable="false" alt="">
						</figure>
					</div>
				</div>
				<div class="respuesta">
					<form id="form" action="enchula.php"  type="get">
						<textarea onkeyup="filtro();" onKeypress="filtro();" maxlength="120" name="respuesta" rows="8" cols="40" placeholder="Ingresa tu respuesta aquí" required="true"></textarea>
						<input type="text" name="nombre" value="" placeholder="Nombre" required="true">
						<input type="text" name="email" value="" placeholder="email" required="true">
						<button type="submit" name="button">Siguiente</button>
					</form>
				</div>
		</div>
		<script type="text/javascript">
			var grocerias = ["guebo","papo ","conoe","cono de","anal ","analmente","semen","mamalo","mámalo","perra","perrita","chavizta","chavita","exite","excite","excitar","exitar","excitación","exitasión","exitación","bollo","tetas","teta","estafa","estafadores","maldito","mardito","malditos","marditos","de mierda","chúpenlo","mámenlo","mámalo","chúpalo","cara de","cagar","qué mierda","vete a la mierda","anda a la mierda","acostarte conmigo","mamarmelo","te coja","tirar conmigo","el guebo","el guevo","las bolas","hija de la gran","hijo de la gran","cabrona","prostituta","imbecil","porno","sexo","lesbiana","ladrones","marico","maricon","marica","homosexual","maricona","puta","puto","estupido","estupida","pendeja","cabron","la concha","orto","elecciones","candidatos","capriles","gobernaciones","indepabis","infierno","imperio","gringo","pudran","pudrir","culo","porqueria","vomistar","vomiestar","robistar","robiestar","choristar","choriestar","trampa","robo","chavez","choro","choros","chavistas","chavista","expropiados","expropiese","expropiacion","expropiaciones","delincuentes","pipe","pipi","totona","cuca","teta","ano","chupen","mamen","chiupen","bolas","desgraciados","chabe","chabez","chabes","chiabe","chiavez","chaves","chiaves","revolucion","robolucion","pito","chupame","mamame","mamar","chupar","escroto","pinga","mamaguebo","mamaguevo","arrastrado","la perra","huele verga","cara de verga","careverga","mama verga","mama vergas","mama guebo","mama pinga","mama pingas","condon","mal nacido","mal parido","mal pario","maricote","maricota","maricones","mámamelo","chúpamelo","coño","guevo","webo"];
			function filtro(){
					var nodo = document.getElementById("form").elements["respuesta"]
					var textarea = nodo.value;
					for(var i = 0; i < grocerias.length;i++){
							regex = new RegExp("(^|\\s)"+grocerias[i]+"($|(?=\\s))","gi")
							textarea = textarea.replace(regex, function($0, $1){return $1 + ""});
					}
					nodo.value = textarea;
			}
			$('.closeda').click(function(){
				$('.modal').fadeIn();
			});
			$('.closedb').click(function(){
				$('.modal').fadeIn();
			});
			$('.closedc').click(function(){
				$('.modal').fadeIn();
			});
		</script>
	</div>
  <footer>
		<div class="modal">
			<section class="alert1">
				<i class="fa fa-close"></i>
				<h1>¡ HEY Bájale 2!</h1>
				<h3>Esta pregunta se activará la semana que viene</h3>
				<img class="vaso" src="images/vaso-completo.png" alt="">
			</section>
		</div>
  </footer>
</body>
</html>
