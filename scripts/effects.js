$(window).load(function() {
  $('.emoji').flexslider({
    animation: "slide",
    animationLoop: false,
    controlNav: false,
    itemWidth: 60,
    itemMargin: 5
  });
});


$(document).ready(function(){
  //modal's
  $('.start').click(function(){
    $('.modal section').addClass('up');
    $('.modal').show('slow');
  })

  $('.fa-close').click(function(){
    $('.modal section').removeClass('up');
    $('.modal').fadeOut();
  });
});
