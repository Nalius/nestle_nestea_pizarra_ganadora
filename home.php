<html>
  <?php include_once('header.php') ?>
  <body>
    <div class="content">
      <div class="hojas-l">
        <img src="images/hojas-izq.png"  draggable="false" alt="">
      </div>
      <div class="hojas-r">
        <img src="images/hojas-der.png"  draggable="false" alt="">
      </div>
      <div class="content-holder">
        <figure class="man"><img src="images/man.png" draggable="false" alt="nestea"></figure>
        <section>
          <img class="titulo" src="images/titulo.png" alt="">
          <p>¿Vas pendiente de sorpresas brutales? Primero, lee las instrucciones</p>
          <button class="start">Instrucciones</button>
          <img class="medio-vaso" src="images/vaso.png" alt="resfrescate frio y natural">
        </section>
      </div>
    </div>
    <footer>
      <div class="modal">
        <section>
          <i class="fa fa-close"></i>
          <header>
            <h1>Instrucciones</h1>
            <p>Antes de empezar, queremos que sepas
              que dando tus respuestas debes ser lo
              más Frío y Natural posible, si crees
              que puedes con esto,</p>
              <h3>¡Sigue Leyendo!</h3>
          </header>
          <div class="instrucciones">
          <div class="inst-holder">
            <img class="p1" src="images/paso1.png" alt="">
            <img class="p2" src="images/paso2.png" alt="">
            <img class="p3" src="images/paso3.png" alt="">
            <img class="p4" src="images/paso4.png" alt="">
            <img class="follow" src="images/follow.png" alt="">
            <img class="recuerda" src="images/recuerda.png" alt="">
          </div>
          </div>
          <img class="vaso" src="images/vaso-completo.png" alt="">
          <img class="listo" src="images/listo.png" alt="">
          <button onClick="window.location.href='preguntas.php' " type="button" name="button">Siguiente</button>
        </section>
      </div>
    </footer>
  </body>
</html>
