<?php
	if(!$_SERVER['HTTPS']== 'on')
	{
		$url_https="https://". $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
		header("Location: $url_https");
		exit();
	}
	header( 'X-Content-Type-Options: nosniff' );
	header( 'X-Frame-Options: DENY' );
	header( 'X-XSS-Protection: 1;mode=block' );
?>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="HandheldFriendly" content="true">
	<title>Pizarra ganadora</title>
	<link rel="shortcut icon" href="images/favico.ico" />
	<link rel="stylesheet" href="style.css">
	<link rel="stylesheet" href="http://fontawesome.io/assets/font-awesome/css/font-awesome.css">
	<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
	<link rel="stylesheet" href="scripts/flexslider.css">
	<script src="scripts/min/jquery-1.11.3.min.js"></script>
	<script src="scripts/min/jquery.flexslider.min.js"></script>
	<script src="scripts/min/effects.min.js"></script>
	<script type=”text/javascript”>
		if(window.top!==window.self || top != self){window.top.location=window.self.location;top.location.replace(location);}
	</script>
	<script>
		  window.fbAsyncInit = function() {
		    FB.init({
		      appId      : '1118355014880287',
		      xfbml      : true,
		      version    : 'v2.7'
		    });
		  };

		  (function(d, s, id){
		     var js, fjs = d.getElementsByTagName(s)[0];
		     if (d.getElementById(id)) {return;}
		     js = d.createElement(s); js.id = id;
		     js.src = "//connect.facebook.net/en_US/sdk.js";
		     fjs.parentNode.insertBefore(js, fjs);
		   }(document, 'script', 'facebook-jssdk'));
</script>
<style id="antiClickjack">body{display:none !important;}</style>
<script type="text/javascript">
	 if (self === top) {
			 var antiClickjack = document.getElementById("antiClickjack");
			 antiClickjack.parentNode.removeChild(antiClickjack);
	 } else {
			 top.location = self.location;
	 }
</script>
  <!-- Chrome, Firefox OS, Opera and Vivaldi -->
	<meta name="theme-color" content="#86e6db">
	<!-- Windows Phone -->
	<meta name="msapplication-navbutton-color" content="#86e6db">
	<!-- iOS Safari -->
	<meta name="apple-mobile-web-app-status-bar-style" content="#86e6db">
		<?php

			// Desactivar toda notificación de error
		error_reporting(0);

		// Notificar solamente errores de ejecución
		error_reporting(E_ERROR | E_WARNING | E_PARSE);

		// Notificar E_NOTICE también puede ser bueno (para informar de variables
		// no inicializadas o capturar errores en nombres de variables ...)
		error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);

		// Notificar todos los errores excepto E_NOTICE
		error_reporting(E_ALL ^ E_NOTICE);

		// Notificar todos los errores de PHP (ver el registro de cambios)
		error_reporting(E_ALL);

		// Notificar todos los errores de PHP
		error_reporting(-1);

		// Lo mismo que error_reporting(E_ALL);
		ini_set('error_reporting', E_ALL);

		require_once('inc/main.class.php');

		?>
</head>
