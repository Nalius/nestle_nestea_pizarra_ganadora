<?php
    /*
    clase de metodos principales
    */
    class Pizarra
    {
        protected $link;
        function __construct()
        {
            require_once('db.php');
        }

        public function verificarUsuario($correo)
        {
          $resultado = $this->link->query("SELECT * FROM `tb_usuarios` WHERE `correo` = '$correo' ");
          if ($resultado->num_rows < 1) {
            return true;
          }else{
            return false;
          }
        }

        public function guardaUsuario($nombre,$correo)
        {
          $resultado = $this->link->query("INSERT INTO `tb_usuarios` (`nombre`,`correo`) VALUES ('$nombre','$correo')"  );
          if ($resultado){
            return true;
          }else{
            return false;
          }
        }

        public function guardarRespuesta($respuesta,$correo)
        {
            $fecha = date('d/m/Y');
            $resultado = $this->link->query("INSERT INTO `tb_respuestas_usuario` (`correo`,`respuesta`,`fecha_respuesta`) VALUES ('$correo','$respuesta','$fecha')"  );
            if ($resultado){
              return true;
            }else{
              return false;
            }
        }

        public function sendMail($msg)
        {
          if (mail("bayomc@gmail.com","Pizara NESTEA",$msg)) {
            return true;
          }else {
            return false;
          }
        }

        public function buscarPregunta()
        {
            $resultado = $this->link->query("SELECT pregunta FROM tb_preguntas WHERE activa = 1 ");
            while($row = $resultado->fetch_object()){
              $a[] = $row;
            }

          return  array('pregunta' => utf8_encode($a[0]->pregunta));

        }

        public function visible()
        {
            $resultado = $this->link->query("SELECT posicion FROM tb_pizarra_visible");
            while($row = $resultado->fetch_object()){
              $a[] = $row;
            }
            return  array('posicion' => $a[0]->posicion);
        }
        
        public function guardaImagen($data,$correo)
        {
            $decodedData=base64_decode($data);
            $rand = rand(0,999999);
            $fic_name =  $correo.'_pizarra_'.date("H:i:s").'.png';
            $fp = fopen('../pizarras/'.$fic_name, 'wb');
            $ok = fwrite($fp, $decodedData);
            fclose($fp);
            if($ok)
                $resultado = $this->link->query("INSERT INTO `tb_pizarra` (`correo`,`pizarra`) VALUES ('$correo','$fic_name')");
                if($resultado){
                  return $fic_name;
                }
              else
                return false;
        }
    }
?>
